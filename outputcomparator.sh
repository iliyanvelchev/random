RED='\033[0;31m'
GREEN='\033[0;32m'
for i in $(ls tests/*.in)
do
    echo $INPUTS
    EXPECTED=$(cat ${i%.*}.sol | tr -d '[:space:]')
    ACTUAL=$(./a.out < $i | tr -d '[:space:]')
    if [ $ACTUAL -eq $EXPECTED ] ;
    then
        echo "${GREEN} ${i%.*} PASS\c"
    else
        echo "${RED} ${i%.*} FAIL\c"
    fi
done