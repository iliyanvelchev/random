package main

import "fmt"

func removeFirstChar(s string) string {
	for i := range s {
		if i > 0 {
			return s[i:]
		}
	}
	return s[:0]
}

type rhash struct {
	value string
	hash  int
}

func (r *rhash) init() {
	if len(r.value) > 0 {
		for i := range r.value {
			r.hash += int(rune(r.value[i]))
		}
	}
}

func (r *rhash) skip() {
	if len(r.value) > 0 {
		firstCode := int([]rune(r.value)[0])
		r.value = removeFirstChar(r.value)
		r.hash = r.hash - firstCode
	}
}
func (r *rhash) append(c string) {
	char := string(c[0])
	newCode := int([]rune(char)[0])
	r.value = r.value + char
	r.hash = r.hash + newCode
}

func (r *rhash) step(c string) {
	r.skip()
	r.append(c)
}

func main() {
	text := "textrandomtext123123123texrand123randomtexttexxtextrandoejqiejwakldm123wkdaldjasj"
	key := "text"
	rs := rhash{value: key}
	rs.init()

	length := len(rs.value)
	counter := 0
	rt := rhash{}
	for i := range text {
		if len(rt.value) < length {
			rt.append(string(text[i]))
		} else {
			rt.step(string(text[i]))
		}

		if rt.hash == rs.hash {
			if rt.value == rs.value {
				counter++
			}
		}

	}
	fmt.Println(counter)
}
