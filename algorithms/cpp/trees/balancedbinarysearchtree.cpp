#include <iostream>
#include <math.h>
#include <list>
using namespace std;

struct Tree
{
    int data;
    int height;
    Tree *parent;
    Tree *left;
    Tree *right;
} *root = nullptr;

//Print every level
const int n = 50;
list<Tree *> ll[n];
void bfs()
{

    int level;
    ll[level].push_back(root);
    while (true)
    {
        list<Tree *>::iterator it;
        for (it = ll[level].begin(); it != ll[level].end(); ++it)
        {
            if ((*it)->left)
                ll[level + 1].push_back((*it)->left);
            if ((*it)->right)
                ll[level + 1].push_back((*it)->right);
        }

        if (!ll[level + 1].empty())
            level++;
        else
            break;
    }
}

void print()
{
    bfs();
    int i = 0;
    while (!ll[i].empty())
    {
        while (!ll[i].empty())
        {
            cout << ll[i].front()->data << " ";
            ll[i].pop_front();
        }
        cout << endl;
        i++;
    }
}

int findHeight(Tree *tree)
{
    if (!tree)
        return -1;
    if (!tree->left && !tree->right)
        return 0;
    return max(findHeight(tree->left) + 1, findHeight(tree->right) + 1);
}

bool isBalanced(Tree *tree)
{
    return abs(findHeight(tree->left) - findHeight(tree->right)) <= 1;
}
int leftHeight(Tree *tree)
{
    return tree->left ? tree->left->height : -1;
}
int rightHeight(Tree *tree)
{
    return tree->right ? tree->right->height : -1;
}

void leftRotation(Tree *tree)
{
    if (!tree || !tree->right)
        return;

    Tree *parent = tree->parent;
    Tree *right = tree->right;
    tree->right = right->left;
    right->left = tree;
    tree->parent = right;
    tree->height = max(leftHeight(tree), rightHeight(tree)) + 1;

    if (parent)
    {
        if (parent->data > tree->data)
            parent->left = right;
        else
            parent->right = right;
        right->parent = parent;
    }
    else
    {
        right->parent = nullptr;
        root = right;
    }
}

void rightRotation(Tree *tree)
{
    if (!tree || !tree->left)
        return;

    Tree *parent = tree->parent;
    Tree *left = tree->left;

    tree->left = left->right;
    left->right = tree;
    tree->parent = left;
    tree->height = max(leftHeight(tree), rightHeight(tree)) + 1;

    if (parent)
    {
        if (parent->data > tree->data)
            parent->left = left;
        else
            parent->right = left;
        left->parent = parent;
    }
    else
    {
        left->parent = nullptr;
        root = left;
    }
}
bool violates(Tree *root)
{
    return !root ? false : abs(leftHeight(root) - rightHeight(root)) > 1;
}
Tree *findLowestViolation(Tree *root)
{
    if (!root)
        return nullptr;
    if (!violates(root) && violates(root->parent))
        return root->parent;
    if (violates(root))
    {
        if (leftHeight(root) > rightHeight(root))
            return findLowestViolation(root->left);
        return findLowestViolation(root->right);
    }
    return nullptr;
}

void balance(Tree *root)
{
    Tree *tree = findLowestViolation(root);
    if (!tree)
        return;

    //Left heavy
    if (leftHeight(tree) - rightHeight(tree) > 1)
    {
        Tree *left = tree->left;
        if (leftHeight(left) >= rightHeight(left))
            rightRotation(tree);
        else
        {
            leftRotation(left);
            rightRotation(tree);
        }
    }
    //Right heavy
    else if (rightHeight(tree) - leftHeight(tree) > 1)
    {
        Tree *right = tree->right;
        if (rightHeight(right) >= leftHeight(right))
            leftRotation(tree);
        else
        {
            rightRotation(right);
            leftRotation(tree);
        }
    }
}

int elements(Tree *root)
{
    if (!root)
        return 0;
    return elements(root->left) + elements(root->right) + 1;
}

void insert(Tree *root, int data)
{
    if (root->data == data)
        return;
    if ((root->data > data && !root->left) || (root->data < data && !root->right))
    {
        Tree *leaf = (Tree *)malloc(sizeof(*root));
        leaf->data = data;
        leaf->parent = root;
        leaf->height = 0;
        leaf->left = leaf->right = nullptr;
        if (root->data > data)
            root->left = leaf;
        else
            root->right = leaf;
    }
    else if (root->data > data)
        insert(root->left, data);
    else if (root->data < data)
        insert(root->right, data);

    root->height = max(leftHeight(root), rightHeight(root)) + 1;
}
void insert(int data)
{
    if (!root)
    {
        root = (Tree *)malloc(sizeof(*root));
        root->height = 0;
        root->data = data;
        root->parent = root->left = root->right = nullptr;
    }
    else
    {
        insert(root, data);
        balance(root);
    }
}
int main()
{
    for (int i = 0; i < 1000000; i++)
    {
        insert(1 + rand() % 1000000);
    }

    if (isBalanced(root))
        cout << "BALANCED" << endl;
    else
        cout << "NOT BALANCED" << endl;

    //print();

    cout << root->data << endl;
    cout << root->height << endl;
    cout << root->left->height << endl;
    cout << root->right->height << endl;

    cout << "ELEMENTS : " << elements(root) << endl;
}
