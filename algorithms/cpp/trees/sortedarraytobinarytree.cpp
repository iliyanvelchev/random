#include <iostream>
#include <list>
using namespace std;

const int n = 10;
int A[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
           13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};

struct Tree
{
    int data;
    Tree *left;
    Tree *right;
} *root = nullptr;

void initialize(int data)
{
    root = (Tree *)malloc(sizeof *root);
    root->left = root->right = nullptr;
    root->data = data;
}
void insert(Tree *tree, int data)
{
    if (!tree)
    {
        initialize(data);
        return;
    }

    if (data > tree->data)
    {
        if (tree->right && tree->right->data)
            insert(tree->right, data);
        else
        {
            Tree *leaf = (Tree *)malloc(sizeof *tree);
            leaf->left = leaf->right = nullptr;
            leaf->data = data;
            tree->right = leaf;
            return;
        }
    }
    else if (data < tree->data)
    {
        if (tree->left && tree->left->data)
            insert(tree->left, data);
        else
        {
            Tree *leaf = (Tree *)malloc(sizeof *tree);
            leaf->left = leaf->right = nullptr;
            leaf->data = data;
            tree->left = leaf;
            return;
        }
    }
}

void constructBinaryTree(int a[], int from, int to)
{
    if (from > to)
        return;

    int middle = (from + to) / 2;
    int element = a[middle];
    insert(root, element);

    constructBinaryTree(a, from, middle - 1);
    constructBinaryTree(a, middle + 1, to);
}

void constructBinaryTree(int a[], int length)
{
    int middle = (length - 1) / 2;
    initialize(a[middle]);
    constructBinaryTree(a, 0, middle - 1);
    constructBinaryTree(a, middle + 1, length - 1);
}

list<Tree *> ll[n];
void bfs()
{

    int level;
    ll[level].push_back(root);
    while (true)
    {
        list<Tree *>::iterator it;
        for (it = ll[level].begin(); it != ll[level].end(); ++it)
        {
            if ((*it)->left)
                ll[level + 1].push_back((*it)->left);
            if ((*it)->right)
                ll[level + 1].push_back((*it)->right);
        }

        if (!ll[level + 1].empty())
            level++;
        else
            break;
    }
}

void print()
{
    int i = 0;
    while (!ll[i].empty())
    {
        while (!ll[i].empty())
        {
            cout << ll[i].front()->data << " ";
            ll[i].pop_front();
        }
        cout << endl;
        i++;
    }
}

int main()
{
    int length = (sizeof(A) / sizeof(*A));
    constructBinaryTree(A, length);
    bfs();
    print();
}