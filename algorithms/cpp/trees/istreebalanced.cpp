#include <iostream>
#define MAXH -10000000;
#define MINH 10000000;
using namespace std;

struct Tree
{
    int data;
    Tree *left;
    Tree *right;
} *root = nullptr;

Tree *initialize(int data)
{
    Tree *root = (Tree *)malloc(sizeof *root);
    root->left = root->right = nullptr;
    root->data = data;
    return root;
}
void insert(Tree *tree, int data)
{
    if (!tree)
    {
        tree = initialize(data);
        return;
    }

    if (data > tree->data)
    {
        if (tree->right && tree->right->data)
            insert(tree->right, data);
        else
        {
            Tree *leaf = (Tree *)malloc(sizeof *tree);
            leaf->left = leaf->right = nullptr;
            leaf->data = data;
            tree->right = leaf;
            return;
        }
    }
    else if (data < tree->data)
    {
        if (tree->left && tree->left->data)
            insert(tree->left, data);
        else
        {
            Tree *leaf = (Tree *)malloc(sizeof *tree);
            leaf->left = leaf->right = nullptr;
            leaf->data = data;
            tree->left = leaf;
            return;
        }
    }
}
int maxHeight = MAXH;
int minHeight = MINH;

void checkBalanced(Tree *tree, int height)
{
    if (!tree->left && !tree->right)
    {
        if (height > maxHeight)
            maxHeight = height;
        if (height < minHeight)
            minHeight = height;
        return;
    }

    if (tree->left)
        checkBalanced(tree->left, height + 1);
    if (tree->right)
        checkBalanced(tree->right, height + 1);
}

bool isBalanced(Tree *tree)
{
    checkBalanced(tree, 0);
    return maxHeight - minHeight <= 1;
}

int main()
{
    root = initialize(10);

    insert(root, 5);
    insert(root, 15);

    insert(root, 3);
    insert(root, 7);
    insert(root, 1);
    insert(root, 4);
    insert(root, 6);
    insert(root, 8);

    insert(root, 12);
    insert(root, 17);
    insert(root, 11);
    insert(root, 13);
    insert(root, 16);
    insert(root, 18);

    if (isBalanced(root))
        cout << "TRUE" << endl;
    else
        cout << "FALSE" << endl;
}