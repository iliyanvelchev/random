#include <iostream>
#include <math.h>

using namespace std;

struct Tree
{
    int data;
    Tree *left;
    Tree *right;
} * rootT1, *rootT2 = nullptr;

Tree *initialize(int data)
{
    Tree *root = (Tree *)malloc(sizeof *root);
    root->left = root->right = nullptr;
    root->data = data;
    return root;
}
void insert(Tree *tree, int data)
{
    if (!tree)
    {
        tree = initialize(data);
        return;
    }

    if (data > tree->data)
    {
        if (tree->right && tree->right->data)
            insert(tree->right, data);
        else
        {
            Tree *leaf = (Tree *)malloc(sizeof *tree);
            leaf->left = leaf->right = nullptr;
            leaf->data = data;
            tree->right = leaf;
            return;
        }
    }
    else if (data < tree->data)
    {
        if (tree->left && tree->left->data)
            insert(tree->left, data);
        else
        {
            Tree *leaf = (Tree *)malloc(sizeof *tree);
            leaf->left = leaf->right = nullptr;
            leaf->data = data;
            tree->left = leaf;
            return;
        }
    }
}

bool matchTree(Tree *t1, Tree *t2)
{
    if (!t1 && !t2)
        return true;
    if (!t1 || !t2)
        return false;
    if (t1->data != t2->data)
        return false;
    return (matchTree(t1->left, t2->left) && matchTree(t1->right, t2->right));
}
bool subTree(Tree *t1, Tree *t2)
{
    if (!t1)
        return false;
    if (t1->data == t2->data)
        return matchTree(t1, t2);
    return (subTree(t1->left, t2) || subTree(t1->right, t2));
}
bool contains(Tree *t1, Tree *t2)
{
    if (!t2)
        return true;
    return subTree(t1, t2);
}
Tree *find(Tree *root, int data)
{
    if (!root)
        return NULL;
    if (root->data == data)
        return root;
    if (root->data > data)
        return find(root->left, data);
    return find(root->right, data);
}

int main()
{
    rootT1 = initialize(100);
    for (int i = 0; i < 50; i++)
    {
        insert(rootT1, 1 + (std::rand() % 200));
    }

    while (rootT2 == NULL)
    {
        rootT2 = find(rootT1, 1 + (std::rand() % 200));
    }

    if (contains(rootT1, rootT2)) //in order to work properly,we must make a deep copy of rootT1
        cout << "TRUE" << endl;
    else
        cout << "FALSE" << endl;
}