#include <iostream>
#include <vector>

struct point
{
    float x;
    float y;
};

struct line
{
    float a;
    float b;
    bool parallel;
};

line compute(point p1, point p2)
{
    line l;
    if (p2.x == p1.x)
        l.parallel = true;
    l.a = !l.parallel ? (p2.y - p1.y) / (p2.x - p1.x) : p1.x;
    l.b = !l.parallel ? (p2.x * p1.y - p1.x * p2.y) / (p2.x - p1.x) : p1.x;
    return l;
}

bool inside(point s, point a, point b, point c)
{
    int asx = s.x - a.x;
    int asy = s.y - a.y;

    bool sab = (b.x - a.x) * asy - (b.y - a.y) * asx > 0;

    if ((c.x - a.x) * asy - (c.y - a.y) * asx > 0 == sab)
        return false;

    if ((c.x - b.x) * (s.y - b.y) - (c.y - b.y) * (s.x - b.x) > 0 != sab)
        return false;

    return true;
}

bool contains(std::vector<point> v, point p, int pc)
{
    std::vector<point>::iterator it;
    int operations = 0;
    for (it = v.begin(); it != v.end(); ++it)
    {
        if (++operations > pc)
            break;
        if ((*it).x == p.x && (*it).y == p.y)
            return true;
    }
    return false;
}

bool contains(std::vector<line> v, line l, int lc)
{
    int operations = 0;
    std::vector<line>::iterator it;
    for (it = v.begin(); it != v.end(); ++it)
    {
        if (++operations > lc)
            break;
        if ((*it).a == l.a && (*it).b == l.b && (*it).parallel == l.parallel)
            return true;
    }
    return false;
}

int main()
{
    std::vector<point> points(4);
    int pc = 0;
    for (int i = 0; i < 4; i++)
    {
        point p;
        std::cin >> p.x >> p.y;
        if (!contains(points, p, pc))
            points[pc++] = p;
    }

    int lc = 0;
    int ops = 0;
    std::vector<line> lines(6);
    for (int i = 0; i < pc - 1; i++)
        for (int j = i + 1; j < pc; j++)
        {
            line l = compute(points[i], points[j]);
            if (!contains(lines, l, lc))
                lines[lc++] = l;
        }

    bool isInside = false;
    int ans = 0;
    point other[3];
    for (int i = 0; i < pc; i++)
    {
        int ix = 0;
        for (int j = 0; j < pc; j++)
        {
            if (i == j)
                continue;
            other[ix++] = points[j];
        }
        isInside = inside(points[i], other[0], other[1], other[2]);
        if (isInside)
            break;
    }

    if (lc == 6 && pc == 4 && !isInside)
        ans = 4;
    else if (lc == 6 && pc == 4 && isInside)
        ans = 3;
    else if (lc == 4)
        ans = 2;
    else if (lc == 3)
        ans = 1;
    std::cout << ans << std::endl;
}