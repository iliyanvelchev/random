#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int main()
{
    int n, k;
    cin >> n >> k;
    vector<int> cs(201);
    vector<int> exp(n);
    for (int i = 0; i < n; i++)
        cin >> exp[i];

    int result = 0;
    for (int i = 0; i < k; i++)
        cs[exp[i]]++;

    for (int i = 0; i < n - k; i++)
    {
        cs[exp[i + k]]++;
        double median = 0;
        int p = k / 2 + 1;
        if (k % 2 == 1)
        {
            for (int d = 200; d >= 0; d--)
            {
                if (median != 0)
                    break;
                if (cs[d] > 0 && p - cs[d] >= 0)
                    p -= cs[d];
                else if (cs[d] > 0)
                    median = (double)d;
            }
        }
        else
        {
            for (int d = 200; d >= 0; d--)
            {
                if (median != 0)
                    break;
                if (p - cs[d] > 0)
                    p -= cs[d];
                else if (p - cs[d] == 0)
                {
                    for (int l = d - 1; l >= 0; l--)
                        if (cs[l] > 0)
                        {
                            median = ((double)d + double(l)) / 2;
                            break;
                        }
                }
                else
                    median = (double)d;
            }
        }
        cs[exp[i]]--;
        if (2 * median <= (double)exp[i + k])
            result++;
    }
    cout << result << endl;
}

// 5 4
// 1 2 3 4 4

// 9 5
// 2 3 4 2 3 6 8 4 5

//2 3 4 2 3 2 2 3 3 4

// 5 3
// 10 20 30 40 50

// 10 8
// 2 6 11 3 4 12 6 8 5 4