#include <iostream>
using namespace std;

int A[9][9] = {0};
int t = 0 ^ 1 ^ 2 ^ 3 ^ 4 ^ 5 ^ 6 ^ 7 ^ 8 ^ 9;

int horizontal(int i)
{
    int m = t;
    int zeros = 0;
    for (int k = 0; k < 9; k++)
    {
        m ^= A[i][k];
        if (A[i][k] == 0)
            zeros++;
    }
    return zeros == 1 ? m : -1;
}

int vertical(int j)
{
    int m = t;
    int zeros = 0;
    for (int k = 0; k < 9; k++)
    {
        m ^= A[k][j];
        if (A[k][j] == 0)
            zeros++;
    }
    return zeros == 1 ? m : -1;
}

int square(int i, int j)
{
    int v = i / 3;
    int h = j / 3;

    int vfrom = v * 3;
    int vto = vfrom + 2;

    int hfrom = h * 3;
    int hto = hfrom + 2;

    int m = t;
    int zeros = 0;

    for (int k = vfrom; k <= vto; k++)
    {
        for (int p = hfrom; p <= hto; p++)
        {
            m ^= A[k][p];
            if (A[k][p] == 0)
                zeros++;
        }
    }
    return zeros == 1 ? m : -1;
}
void solve(int i, int j)
{
    if (A[i][j] != 0)
        return;
    int h = horizontal(i);
    if (h != -1)
    {
        A[i][j] = h;
        return;
    }
    int v = vertical(j);
    if (v != -1)
    {
        A[i][j] = v;
        return;
    }
    int s = square(i, j);
    if (s != -1)
        A[i][j] = s;
}
int main()
{
    int zeros = 0;
    for (int i = 0; i < 9; i++)
        for (int j = 0; j < 9; j++)
        {
            cin >> A[i][j];
            if (A[i][j] == 0)
                zeros++;
        }

    while (zeros != 0)
        for (int i = 0; i < 9; i++)
            for (int j = 0; j < 9; j++)
            {
                if (A[i][j] == 0)
                {
                    solve(i, j);
                    if (A[i][j] != 0)
                        zeros--;
                }
            }

    for (int i = 0; i < 9; i++)
    {
        for (int j = 0; j < 9; j++)
            cout << A[i][j] << " ";
        if (i != 8)
            cout << endl;
    }
}
