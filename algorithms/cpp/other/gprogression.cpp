#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;
int main()
{
    long long n, r, result = 0;
    cin >> n >> r;
    unordered_map<long long, long long> lm;
    unordered_map<long long, long long> rm;
    vector<long long> arr(n);
    for (int t = 0; t < n; t++)
    {
        long long e;
        cin >> e;
        arr[t] = e;
        rm[e]++;
    }
    for (long long val : arr)
    {
        rm[val]--;
        int li = val % r == 0 ? val / r : 0;
        int ri = val * r;
        result += rm[ri] * lm[li];
        lm[val]++;
    }

    cout << result << endl;
}