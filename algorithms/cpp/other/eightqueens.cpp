#include <iostream>

using namespace std;
int positions = 0;
void print(int a[8][8])
{
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            if (a[i][j] == 0)
                cout << "_ ";
            else
                cout << "Q ";
        }
        cout << endl;
    }
    cout << endl;
    cout << endl;
    cout << endl;
}
//4 8 16 18 18 16 8 4

bool checkV(int y, int a[8][8])
{
    for (int i = 0; i < 8; i++)
        if (a[i][y])
            return false;
    return true;
}

bool checkD1(int x, int y, int a[8][8])
{
    for (int i = x, j = y; i >= 0 && j >= 0; i--, j--)
        if (a[i][j])
            return false;
    return true;
}

bool checkD2(int x, int y, int a[8][8])
{
    for (int i = x, j = y; i >= 0 && j < 8; i--, j++)
        if (a[i][j])
            return false;
    return true;
}

bool checkAll(int x, int y, int a[8][8])
{
    return checkV(y, a) && checkD1(x, y, a) && checkD2(x, y, a);
}

void queens(int x, int a[8][8])
{
    if (x > 7)
    {
        positions++;
        print(a);
    }

    for (int i = 0; i < 8; i++)
        if (checkAll(x, i, a))
        {
            a[x][i] = 1;
            queens(x + 1, a);
            a[x][i] = 0;
        }
}
int main()
{
    int a[8][8] = {};
    queens(0, a);
    cout << positions << endl;
}