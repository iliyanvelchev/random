#include <iostream>

const int size = 10;
int a[size] = {5, 4, 2, 3, 4, 5, 6, 7, 1, 2};

void merge(int *a, int start, int middle, int end)
{
    int n = end - start + 1;
    int b[n], operations = 0;
    int i = start, j = middle + 1;

    while (i <= middle && j <= end)
        if (a[i] > a[j])
            b[operations++] = a[j++];
        else
            b[operations++] = a[i++];

    while (i <= middle)
        b[operations++] = a[i++];
    while (j <= end)
        b[operations++] = a[j++];

    for (j = 0; j < n; j++)
        a[start + j] = b[j];
}
void mergesort(int *a, int start, int end)
{
    if (start >= end)
        return;
    int middle = (start + end) / 2;
    mergesort(a, start, middle);
    mergesort(a, middle + 1, end);
    merge(a, start, middle, end);
}

int main()
{
    mergesort(a, 0, size - 1);
    for (int i = 0; i < size - 1; i++)
        std::cout << a[i] << " ";
}