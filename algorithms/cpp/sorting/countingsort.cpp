#include <iostream>
#define MAX_SIZE 100

int output[MAX_SIZE];
int count[MAX_SIZE];
const int n = 10;
int a[n] = {3, 5, 2, 3, 1, 2, 3, 4, 5, 6};
int main()
{
    for (int i = 0; i < n; i++)
    {
        if (a[i] >= 0 && a[i] < MAX_SIZE)
            count[a[i]]++;
    }

    int index = 0;
    for (int i = 0; i < n; i++)
        if (count[i] != 0)
            for (int j = 0; j < count[i]; j++)
                output[index++] = i;

    for (int i = 0; i < n; i++)
        std::cout << output[i] << " ";
}