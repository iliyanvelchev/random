#include <iostream>
#include <queue>

#define MAX_VALUE 100000000
using namespace std;

const int n = 6;
const int s = 0;
const int t = 5;
// int A[n][n] = {{0, 5, 5, 10, 0, 0},
//                {0, 0, 4, 0, 0, 5},
//                {0, 0, 0, 0, 7, 0},
//                {0, 0, 0, 0, 0, 7},
//                {0, 0, 0, 0, 0, 8},
//                {0, 0, 0, 0, 0, 0}};

int A[n][n] = {{0, 16, 13, 0, 0, 0},
               {0, 0, 10, 12, 0, 0},
               {0, 4, 0, 0, 14, 0},
               {0, 0, 9, 0, 0, 20},
               {0, 0, 0, 7, 0, 4},
               {0, 0, 0, 0, 0, 0}};

int F[n][n];
int path[n];
int used[n];
int minCap = MAX_VALUE;
bool found = false;

int BFS(int s, int t, int level)
{
    queue<int> q;
    int pred[n];
    int rp[n];
    q.push(s);

    for (int i = 0; i < n; i++)
        pred[i] = -1;

    while (!q.empty())
    {
        int e = q.front();
        used[e] = 1;
        q.pop();

        for (int i = 0; i < n; i++)
            if (A[e][i] > 0 && !used[i])
            {
                pred[i] = e;
                if (i == t)
                {
                    rp[level++] = t;
                    int p = pred[t];
                    if (minCap > A[p][t])
                        minCap = A[p][t];
                    while (p >= 0)
                    {
                        rp[level++] = p;
                        if (minCap > A[pred[p]][p])
                            minCap = A[pred[p]][p];
                        p = pred[p];
                    }
                    for (int i = 0; i < level - 1; i++)
                        path[level - i - 1] = rp[i];
                    found = true;
                    return level - 1;
                }
                q.push(i);
            }
    }
    found = false;
    return 0;
}

int findAugmentingPath(int s, int t)
{
    int length = BFS(s, t, 0);
    for (int i = 0; i < n; i++)
        used[i] = 0;
    return length + 1;
}
int findMaxFlow(int s, int t)
{
    int length = findAugmentingPath(s, t);
    while (found)
    {
        for (int i = 0; i < length; i++)
            cout << path[i] + 1 << " ";
        cout << endl;
        for (int i = 0; i < length - 1; i++)
        {
            int from = path[i];
            int to = path[i + 1];
            F[from][to] += minCap;
            F[to][from] -= minCap;
            A[from][to] -= minCap;
            A[to][from] += minCap;
        }
        minCap = MAX_VALUE;
        length = findAugmentingPath(s, t);
    }

    int flow = 0;
    for (int i = 0; i < n; i++)
    {
        cout << F[s][i] << " ";
        flow += F[s][i];
    }
    cout << endl;
    return flow;
}
int main()
{
    int maxFlow = findMaxFlow(s, t);
    cout << maxFlow << endl;
}