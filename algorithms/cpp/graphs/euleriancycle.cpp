#include <iostream>

using namespace std;

const int n = 8;
int A[n][n] = {
    {0, 1, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0},
    {0, 0, 0, 1, 1, 0, 1, 0},
    {0, 1, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0}};

void findEulerianCycle(int i)
{
    int stack[n * n], cStack[n * n];
    int sTop = -1;
    int cTop = -1;
    int k = 0;
    stack[sTop++] = i;
    while (sTop > -1)
    {
        int element = stack[sTop];
        for (k = 0; k < n; k++)
            if (A[element][k])
            {
                stack[++sTop] = k;
                A[element][k] = 0;
                break;
            }
        if (k >= n)
            cStack[++cTop] = stack[sTop--];
    }
    for (int k = cTop; k >= 0; k--)
        cout << cStack[k] + 1 << " ";
    cout << endl;
}

bool isEulerianGraph()
{
    for (int i = 0; i < n; i++)
    {
        int in = 0;
        int out = 0;
        for (int j = 0; j < n; j++)
        {
            if (A[i][j])
                out++;
            if (A[j][i])
                in++;
        }
        if (in != out)
            return false;
    }
    return true;
}
int main()
{
    if (isEulerianGraph())
        findEulerianCycle(0);
    else
        cout << "Not an eulerian graph";
}