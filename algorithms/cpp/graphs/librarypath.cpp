#include <iostream>
#include <vector>
using namespace std;

long long size = 0;
void dfs(long long edge, vector<vector<long long>> &graph, vector<long long> &used)
{
    used[edge] = 1;
    size++;
    for (long long i = 0; i < graph[edge].size(); i++)
        if (!used[graph[edge][i]])
            dfs(graph[edge][i], graph, used);
}
int main()
{
    long long q, n, m, l, r, x, y;
    cin >> q;
    for (long long i = 0; i < q; i++)
    {
        cin >> n >> m >> l >> r;
        vector<vector<long long>> graph(n);
        vector<long long> used(n, 0);
        for (long long j = 0; j < m; j++)
        {
            cin >> x >> y;
            x--, y--;
            graph[x].push_back(y);
            graph[y].push_back(x);
        }
        if (l <= r)
            cout << n * l << endl;
        else
        {
            long long cost = 0;
            for (long long k = 0; k < n; k++)
            {
                if (!used[k])
                {
                    dfs(k, graph, used);
                    cost += l + (size - 1) * r;
                    size = 0;
                }
            }
            cout << cost << endl;
        }
    }
}
// 37576437 5399224 3804450 154772704 7087608 23974067 23687032 580930 21960708 22517014
// 37165913 5329988 3804450 154772704 7087608 23974067 23687032 224470 20884425 22106767
