#include <iostream>
using namespace std;
int used[10][10];
int A[10][10];
int n, m;
int x[8] = {-1, -1, -1, 0, 0, 1, 1, 1};
int y[8] = {-1, 0, 1, -1, 1, -1, 0, 1};
int step = 0;
void dfs(int i, int j)
{
    used[i][j] = ++step;
    for (int k = 0; k < 8; k++)
        if (i + x[k] >= 0 && j + y[k] >= 0 && i + x[k] < n && j + y[k] < m &&
            A[i + x[k]][j + y[k]] && !used[i + x[k]][j + y[k]])
            dfs(i + x[k], j + y[k]);
}
int main()
{
    cin >> n >> m;
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            cin >> A[i][j];
    int ans = 0;
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            if (A[i][j] && !used[i][j])
            {
                dfs(i, j);
                if (step > ans)
                    ans = step;
                step = 0;
            }
    cout << ans << endl;
}
