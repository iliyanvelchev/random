#include <iostream>
#include <queue>
using namespace std;

const int n = 14;

const char A[n][n] = {
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1},
    {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1},
    {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0}};

int pred[n];
int used[n];

void bfs(int v)
{
    queue<int> q;
    q.push(v);
    used[v] = 1;
    pred[v] = -1;

    while (!q.empty())
    {
        int element = q.front();
        q.pop();

        for (int i = 0; i < n; i++)
        {
            if (A[element][i] != 0 && used[i] == 0)
            {
                used[i] = 1;
                pred[i] = element;
                q.push(i);
            }
        }
    }
}

int printPath(int j)
{
    int count = 1;
    if (pred[j] > -1)
        count += printPath(pred[j]);

    cout << j + 1 << " ";
    return count;
}

void findShortestPathBetween(int i, int j)
{
    bfs(i);
    if (pred[j] == 0)
        cout << "No path" << endl;
    else
    {
        int count = printPath(j);
        cout << endl;
        cout << "Length : " << count << endl;
    }
}

int main()
{
    findShortestPathBetween(0, 9);
}