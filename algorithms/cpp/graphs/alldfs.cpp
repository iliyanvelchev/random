#include <iostream>
using namespace std;

const unsigned n = 14;
const char A[n][n] = {
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1},
    {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1},
    {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0}};

char used[n];
int path[n];
int indx = 0;

void print()
{
    for (int i = 0; i <= indx; i++)
        cout << path[i] + 1 << " ";
    cout << endl;
}
void allDfs(int start, int end)
{
    if (start == end)
    {
        path[indx] = end;
        print();
        return;
    }
    used[start] = 1;
    path[indx++] = start;

    for (int i = 0; i < n; i++)
    {
        if (A[start][i] != 0 && used[i] == 0)
        {
            allDfs(i, end);
            used[start] = 0;
            indx--;
        }
    }
}
int main()
{
    allDfs(0, 9);
}