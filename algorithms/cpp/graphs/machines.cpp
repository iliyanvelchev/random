#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <set>
#include <math.h>
using namespace std;
typedef pair<int, int> pi;

int n, m, ans = 0;
priority_queue<pair<int, pi>> pq;
set<int> machines;
map<int, int> parent;
int getRoot(int i)
{
    int root = i;
    while (parent[root] != root)
        root = parent[root];

    int savei;
    while (parent[i] != i)
    {
        savei = i;
        i = parent[i];
        parent[savei] = root;
    }
    return root;
}
void kruskal(vector<int> mc)
{
    for (int i = 0; i < n - 1; i++)
    {
        pair<int, pi> p = pq.top();
        pq.pop();
        int r1 = getRoot(p.second.first);
        int r2 = getRoot(p.second.second);
        if (r1 != r2)
        {
            if (mc[r1] && mc[r2])
                ans += p.first;
            else
            {
                mc[r1] = max(mc[r1], mc[r2]);
                parent[r2] = r1;
            }
        }
    }
}
int main()
{
    cin >> n >> m;
    for (int i = 0; i < n - 1; i++)
    {
        int x, y, z;
        cin >> x >> y >> z;
        pq.push(make_pair(z, make_pair(x, y)));
        parent[x] = x;
        parent[y] = y;
    }
    vector<int> mc(n);
    for (int i = 0; i < m; i++)
    {
        int machine;
        cin >> machine;
        mc[machine] = 1;
    }
    kruskal(mc);
    cout << ans << endl;
}