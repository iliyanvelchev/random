#include <iostream>
using namespace std;

const int n = 6;
const int A[n][n] = {
    {0, 5, 0, 0, 7, 7},
    {5, 0, 5, 0, 0, 0},
    {0, 5, 0, 6, 5, 0},
    {0, 0, 6, 0, 3, 3},
    {7, 0, 5, 3, 0, 5},
    {7, 0, 0, 3, 5, 0}};

int used[n];
int minCycle[n + 1];
int cycle[n + 1];
int minWeigth = 1000000;
int weigth = 0;
void hamilton(int node, int level)
{
    if (node == 0 && level > 1)
    {
        if (level == n + 1)
        {
            minWeigth = weigth;
            for (int k = 1; k <= n; k++)
                minCycle[k] = cycle[k];
        }
        return;
    }
    if (used[node])
        return;
    used[node] = 1;
    for (int i = 0; i < n; i++)
        if (A[node][i] && node != i)
        {
            weigth += A[node][i];
            cycle[level] = i;
            if (weigth < minWeigth)
                hamilton(i, level + 1);
            weigth -= A[node][i];
        }
    used[node] = 0;
}
int main()
{
    cycle[0] = 0;
    hamilton(0, 1);

    for (int i = 0; i <= n; i++)
        cout << minCycle[i] + 1 << " ";
    cout << endl;
    cout << minWeigth << endl;
}