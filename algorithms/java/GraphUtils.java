package graphs;

public class GraphUtils
{
		private static int[][] UNWEIGHTED = {
				{ 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0 },
				{ 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0 },
				{ 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1 },
				{ 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 },
				{ 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 },
				{ 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0 } };

		private static Integer WEIGHTED[][] = {
				{ 0, 23, 0, 0, 0, 0, 0, 8, 0, 0 },
				{ 23, 0, 0, 3, 0, 0, 34, 0, 0, 0 },
				{ 0, 0, 0, 6, 0, 0, 0, 25, 0, 7 },
				{ 0, 3, 6, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 10, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 10, 0, 0, 0, 0, 0 },
				{ 0, 34, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 8, 0, 25, 0, 0, 0, 0, 0, 0, 30 },
				{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 7, 0, 0, 0, 0, 30, 0, 0 }
		};

		public static Integer[][] getWeightedGraph(Integer value)
		{
				Integer[][] result = new Integer[WEIGHTED.length][WEIGHTED.length];
				for (int i = 0; i < WEIGHTED.length; i++)
						for (int j = 0; j < WEIGHTED.length; j++)
								if (WEIGHTED[i][j] == 0)
										result[i][j] = value;
								else
										result[i][j] = WEIGHTED[i][j];
				return result;
		}

		public static int[][] getUnweightedGraph()
		{
				return UNWEIGHTED;
		}
}
