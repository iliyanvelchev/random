package graphs;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class DijkstraCurrencyConverter
{
		private static int N = 4;

		private static Double[][] relations = new Double[N][N];

		private static Double[] sp = new Double[N];

		private static String[] names = { "EUR", "USD", "GBP", "JPY" };

		private static PriorityQueue<Pair> pq = new PriorityQueue<>();

		private static int[] pred = new int[N];

		private static int getId(String currency)
		{
				switch (currency)
				{
						case "EUR":
								return 0;
						case "USD":
								return 1;
						case "GBP":
								return 2;
						case "JPY":
						default:
								return 3;
				}
		}

		public static void main(String[] args)
		{
				Map<String, Double> rates = new HashMap<>();
				rates.put("EUR/USD", 1.1380);
				rates.put("EUR/GBP", 0.8888);
				rates.put("EUR/JPY", 128.203);
				rates.put("GBP/USD", 1.2802);
				rates.put("GBP/JPY", 144.203);
				rates.put("USD/JPY", 112.721);

				for (String key : rates.keySet())
				{
						String[] pairs = key.split("/");
						int first = getId(pairs[0]);
						int second = getId(pairs[1]);
						relations[first][first] = 1d;
						relations[second][second] = 1d;
						relations[first][second] = rates.get(key);
						relations[second][first] = 1 / relations[first][second] + 0.01;
				}

				dijkstra(getId("EUR"));

				print(getId("EUR"));
		}

		private static void print(int key)
		{
				for (int i = 0; i < N; i++)
				{
						System.out.println(names[key] + "/" + names[i] + " : " + sp[i]);
						if (pred[i] == -1)
								System.out.println("UNREACHABLE");
						else
						{
								System.out.print(names[key] + " ");
								print(key, i);
								System.out.println();
						}
				}
		}

		private static void print(int start, int key)
		{
				if (pred[key] != start)
						print(start, pred[key]);
				System.out.print(names[key] + " ");
		}

		private static void dijkstra(int start)
		{
				for (int i = 0; i < N; i++)
				{
						sp[i] = relations[start][i];
						double length = relations[start][i];
						if (i != start)
								if (length != 0)
								{
										pred[i] = start;
										pq.add(new Pair(i, relations[start][i]));
								}
								else
								{
										pred[i] = -1;
										pq.add(new Pair(i, Double.MAX_VALUE));
								}
				}

				while (!pq.isEmpty())
				{
						relax(pq.poll().key);
				}

		}

		private static void relax(Integer key)
		{
				for (int i = 0; i < N; i++)
				{
						if (!overflows(i, key) && sp[key].compareTo(sp[i] * relations[i][key]) == 1)
						{
								sp[key] = sp[i] * relations[i][key];
								pred[key] = i;
						}
				}
		}

		private static boolean overflows(int i, Integer key)
		{
				return sp[i].compareTo(Double.MAX_VALUE) == 0 || relations[i][key].compareTo(Double.MAX_VALUE) == 0;
		}

		private static class Pair implements Comparable<Pair>
		{
				Integer key;

				Double value;

				public Pair(int key, double value)
				{
						this.key = key;
						this.value = value;
				}

				@Override
				public int compareTo(Pair o)
				{
						return value.compareTo(o.value);
				}
		}
}