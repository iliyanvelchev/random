package graphs;

import java.util.ArrayList;

public class AllDfs
{
		static int length = 0;

		private static int[][] A = GraphUtils.getUnweightedGraph();

		private static int N = A.length;

		public static ArrayList<Integer> path = new ArrayList();

		public static void main(String[] args)
		{
				int i = 0, j = 9;
				allDfs(i, j);
		}

		private static void allDfs(int i, int j)
		{
				path.add(length++, i);

				if (i == j)
				{
						printPath();
						return;
				}
				for (int neighbour = 0; neighbour < N; neighbour++)
				{
						if (A[i][neighbour] != 0 && !path.contains(neighbour))
						{
								allDfs(neighbour, j);
								path.remove(--length);
						}
				}
		}

		private static void printPath()
		{
				System.out.print("Path : ");
				for (int i = 0; i < path.size(); i++)
				{
						System.out.print(path.get(i) + 1 + " ");
				}
				System.out.println();
		}
}
