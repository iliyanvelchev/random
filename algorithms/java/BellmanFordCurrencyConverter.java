package graphs;

import java.util.HashMap;
import java.util.Map;

public class BellmanFordCurrencyConverter
{
		public static int N = 4;

		public static Double[][] C = new Double[N][N];

		public static Double[] D = new Double[N];

		public static String[] names = { "EUR", "USD", "GBP", "BGN" };

		public static String[][] paths = new String[N][N];

		public static int getId(String currency)
		{
				switch (currency)
				{
						case "EUR":
								return 0;
						case "USD":
								return 1;
						case "GBP":
								return 2;
						case "BGN":
						default:
								return 3;
				}
		}

		public static void main(String[] args)
		{
				Map<String, Double> rates = new HashMap<>();
				rates.put("EUR/USD", 1.1380);
				rates.put("EUR/GBP", 0.8888);
				rates.put("EUR/JPY", 128.203);
				rates.put("GBP/USD", 1.2802);
				rates.put("GBP/JPY", 144.203);
				rates.put("USD/JPY", 112.721);

				for (String key : rates.keySet())
				{
						String[] pairs = key.split("/");
						int first = getId(pairs[0]);
						int second = getId(pairs[1]);
						C[second][second] = 1d;
						C[first][second] = rates.get(key);
						C[first][first] = 1d;
						C[second][first] = 1 / C[first][second] + 0.01;
				}

				bellmanFord(getId("EUR"));
				print(getId("EUR"));

		}

		private static void print(int id)
		{
				//bellmanFord(id);

				for (int i = 0; i < N; i++)
				{
						System.out.println(names[id] + "/" + names[i] + " : " + D[i]);
						for (int j = 0; j < N; j++)
								if (paths[i][j] != null)
										System.out.println(paths[i][j]);
				}
		}

		private static void bellmanFord(int id)
		{
				for (int i = 0; i < N; i++)
						if (C[id][i] != 0)
								D[i] = C[id][i];
						else
								C[id][i] = Double.MAX_VALUE;
				print(id);
				System.out.println();
				for (int n = 0; n < N - 2; n++)
						for (int i = 0; i < N; i++)
								for (int j = 0; j < N; j++)
										if (!overflows(D[j], C[j][i]) && D[i].compareTo(D[j] * C[j][i]) == 1)
										{
												D[i] = D[j] * C[j][i];
												paths[i][j] = names[j] + "->" + names[i];
										}
		}

		private static boolean overflows(Double first, Double second)
		{
				Double max = Double.MAX_VALUE;
				return first.compareTo(max) == 0 || second.compareTo(max) == 0;
		}
}
