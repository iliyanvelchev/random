#include <iostream>
#include <list>

using namespace std;
const int n = 10;

struct Tree
{
    int data;
    Tree *left;
    Tree *right;
} *root = nullptr;

void initialize(int data)
{
    root = (Tree *)malloc(sizeof *root);
    root->left = root->right = nullptr;
    root->data = data;
}
void insert(Tree *tree, int data)
{
    if (!tree)
    {
        initialize(data);
        return;
    }

    if (data > tree->data)
    {
        if (tree->right && tree->right->data)
            insert(tree->right, data);
        else
        {
            Tree *leaf = (Tree *)malloc(sizeof *tree);
            leaf->left = leaf->right = nullptr;
            leaf->data = data;
            tree->right = leaf;
            return;
        }
    }
    else if (data < tree->data)
    {
        if (tree->left && tree->left->data)
            insert(tree->left, data);
        else
        {
            Tree *leaf = (Tree *)malloc(sizeof *tree);
            leaf->left = leaf->right = nullptr;
            leaf->data = data;
            tree->left = leaf;
            return;
        }
    }
}
list<Tree *> ll[n];
void bfs()
{

    int level;
    ll[level].push_back(root);
    while (true)
    {
        list<Tree *>::iterator it;
        for (it = ll[level].begin(); it != ll[level].end(); ++it)
        {
            if ((*it)->left)
                ll[level + 1].push_back((*it)->left);
            if ((*it)->right)
                ll[level + 1].push_back((*it)->right);
        }

        if (!ll[level + 1].empty())
            level++;
        else
            break;
    }
}

void print()
{
    int i = 0;
    while (!ll[i].empty())
    {
        while (!ll[i].empty())
        {
            cout << ll[i].front()->data << " ";
            ll[i].pop_front();
        }
        cout << endl;
        i++;
    }
}
int main()
{

    // insert(root, 10);

    // insert(root, 5);
    // insert(root, 15);

    // insert(root, 3);
    // insert(root, 7);
    // insert(root, 1);
    // insert(root, 4);
    // insert(root, 6);
    // insert(root, 8);

    // insert(root, 12);
    // insert(root, 17);
    // insert(root, 11);
    // insert(root, 13);
    // insert(root, 16);
    // insert(root, 18);

    for (int i = 1; i < 10; i++)
    {
        insert(root, i);
    }

    bfs();
    print();
}