#include <stdio.h>
#include <stdlib.h>

class LinkedList
{
  public:
    int data;
    LinkedList *next;
} * head;

void listInit()
{
    head = (LinkedList *)malloc(sizeof *head);
    head->next = NULL;
}

void listInsert(int data)
{
    LinkedList *p, *q, *r;
    q = (LinkedList *)malloc(sizeof *head);
    r = (p = head)->next;
    while (r != NULL)
    {
        p = r;
        r = r->next;
    }
    q->data = data;
    q->next = r;
    p->next = q;
}
LinkedList *listSearch(int data)
{
    LinkedList *q;
    for (q = head->next; q != NULL; q = q->next)
    {
        if (q->data == data)
        {
            return NULL;
        }
    }
    return NULL;
}
void listPrint(void)
{
    LinkedList *q;
    char buf[9];
    for (q = head->next; q != NULL; q = q->next)
    {
        sprintf(buf, "%d", q->data);
        printf("%8s", buf);
    }
}

void removeAlike(LinkedList *p)
{
    if (p == NULL || p->next == NULL)
        return;

    LinkedList *q = p;
    while (q->next != NULL)
    {
        if (q->next->data == p->data)
        {
            LinkedList *r = q->next;
            q->next = q->next->next;
            free(r);
        }
        else
        {
            q = q->next;
        }
    }
}

void removeDuplicates()
{
    LinkedList *q;
    for (q = head->next; q != NULL; q = q->next)
    {
        removeAlike(q);
    }
}

int main()
{
    listInit();
    listInsert(1);
    listInsert(2);
    listInsert(2);
    listInsert(2);
    listInsert(2);
    listInsert(3);
    listInsert(3);
    listInsert(4);
    listInsert(4);
    listInsert(4);
    listInsert(4);
    listInsert(4);
    listInsert(4);
    listInsert(4);
    listInsert(5);
    removeDuplicates();
    listPrint();
}