#include <iostream>
#include <stack>

#define MAX -1000000000

using namespace std;

void sortStack(stack<int> &sorted)
{
    stack<int> helper;
    int size = sorted.size();
    int fixed = 0;
    while (fixed < size)
    {
        int max = MAX;
        while (sorted.size() > fixed)
        {
            if (sorted.top() > max)
                max = sorted.top();

            helper.push(sorted.top());
            sorted.pop();
        }

        sorted.push(max);
        fixed++;

        bool checked = false;
        while (!helper.empty())
        {
            if (helper.top() == max && !checked)
            {
                helper.pop();
                checked = true;
            }
            else
            {
                sorted.push(helper.top());
                helper.pop();
            }
        }
    }
}

int main()
{
    stack<int> s;

    s.push(1);
    s.push(6);
    s.push(3);
    s.push(8);
    s.push(8);
    s.push(9);
    s.push(11);
    s.push(13);

    sortStack(s);

    while (!s.empty())
    {
        cout << s.top() << endl;
        s.pop();
    }
}