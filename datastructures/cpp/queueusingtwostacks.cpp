#include <iostream>
#include <stack>

using namespace std;

class Queue
{
  private:
    stack<int> first, second;

    bool transfer()
    {
        int firstSize = first.size();
        while (!first.empty())
        {
            second.push(first.top());
            first.pop();
        }
        return firstSize != 0;
    }

  public:
    void push(int a)
    {
        if (a >= 0)
        {
            first.push(a);
            return;
        }
        cout << "The element must be >= 0" << endl;
    }
    int top()
    {
        if (second.empty())
        {
            if (transfer())
                return second.top();

            cout << "Queue is empty" << endl;
            return -1;
        }
        return second.top();
    }
    void pop()
    {
        if (!second.empty())
        {
            second.pop();
            return;
        }
        if (transfer())
        {
            second.pop();
            return;
        }
        cout << "Queue is empty" << endl;
    }
    int size()
    {
        return first.size() + second.size();
    }
    bool empty()
    {
        return first.empty() && second.empty();
    }
} queue;

int main()
{
    queue.top();
    cout << "SIZE : " << queue.size() << endl;

    for (int i = 1; i <= 10; i++)
    {
        queue.push(i);
    }
    cout << "SIZE : " << queue.size() << endl;

    while (!queue.empty())
    {
        cout << queue.top() << endl;
        queue.pop();
    }

    queue.pop();
    cout << "SIZE : " << queue.size() << endl;
}