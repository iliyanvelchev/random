#include <stdio.h>
#include <stdlib.h>
#include <set>
#include <iostream>

using namespace std;

struct LinkedList
{
    int data;
    LinkedList *next;
} * head;

void initializeList()
{
    head = (struct LinkedList *)malloc(sizeof *head);
    head->next = NULL;
}

void add(int data)
{
    LinkedList *k, *e;

    e = (struct LinkedList *)malloc(sizeof *head);
    e->data = data;
    e->next = NULL;

    k = head;
    while (k->next != NULL)
        k = k->next;
    k->next = e;
}

void printList()
{
    LinkedList *k = head->next;
    char buf[9];
    while (k != NULL)
    {
        sprintf(buf, "%d", k->data);
        printf("%8s", buf);
        k = k->next;
    }
}

int findCycle(LinkedList *k)
{
    if (k == NULL || k->next == NULL)
    {
        return 0;
    }
    LinkedList *p = k;
    set<int> setOfNodes;
    while (k->next != NULL)
    {
        if (setOfNodes.find(k->next->data) != setOfNodes.end())
        {
            return k->next->data;
        }
        setOfNodes.insert(k->next->data);
        k = k->next;
    }
    return 0;
}
int main()
{
    initializeList();
    add(1);
    add(2);
    add(3);
    add(4);
    add(1);
    printList();
    cout << endl;
    cout << findCycle(head) << endl;
}