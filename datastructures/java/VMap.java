package tasks.versionmap;

import java.util.LinkedHashMap;

public interface VMap<String, V>
{
		LinkedHashMap<Long, V> getAll(String key);

		V getElement(String key);

		V getElement(String key, long timestamp);

		void putElement(String key, V value);

		void putElement(String key, V value, long timestamp);

		void removeElement(String key);

		void removeElement(String key, long timestamp);
}
