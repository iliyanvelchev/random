package tasks.versionmap;

import org.joda.time.DateTime;

import java.util.*;

public class VersionMap<String, V> implements VMap<String, V>
{
		private Map<String, LinkedHashMap<Long, V>> map = new HashMap<>();

		public LinkedHashMap<Long, V> getAll(String key)
		{
				return map.get(key);
		}

		public V getElement(String key)
		{
				return getElement(key, DateTime.now().getMillis());
		}

		public V getElement(String key, long timestamp)
		{
				LinkedHashMap<Long, V> history = map.get(key);
				if (history != null)
				{
						Long returnKey = getKeyByTimestamp(history.keySet(), timestamp);
						if (returnKey != null)
								return history.get(returnKey);
				}
				return null;
		}

		public void putElement(String key, V value)
		{
				putElement(key, value, DateTime.now().getMillis());
		}

		public void putElement(String key, V value, long timestamp)
		{
				LinkedHashMap<Long, V> history = map.get(key);
				if (history != null)
				{
						history.put(timestamp, value);
				}
				else
				{
						history = new LinkedHashMap();
						history.put(timestamp, value);
						map.put(key, history);
				}
		}

		public void removeElement(String key)
		{
				map.remove(key);
		}

		public void removeElement(String key, long timestamp)
		{
				LinkedHashMap<Long, V> history = map.get(key);
				if (history != null)
				{
						Long removeKey = getKeyByTimestamp(history.keySet(), timestamp);

						if (removeKey != null)
								history.remove(removeKey);
				}
		}

		private Long getKeyByTimestamp(Set<Long> keys, long timestamp)
		{
				Long[] vkeys = keys.toArray(new Long[] {});
				for (int i = vkeys.length - 1; i >= 0; i--)
						if (timestamp > vkeys[i])
								return vkeys[i];

				return null;
		}

		public void print()
		{
				for (Map.Entry<String, LinkedHashMap<Long, V>> entry : map.entrySet())
				{
						for (Map.Entry<Long, V> hEntry : entry.getValue().entrySet())
						{
								System.out.println("Key : " + entry.getKey() + " Value : " + hEntry.getValue() + " Timestamp : " + hEntry.getKey());
						}
						System.out.println();
				}
		}
}
