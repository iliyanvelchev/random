package tasks.versionmap;

import org.joda.time.DateTime;

public class VMapDemo
{
		public static void main(String[] args) throws InterruptedException
		{
				VersionMap<String, String> vmap = new VersionMap<>();

				vmap.putElement("1", "first");
				Thread.sleep(500);
				vmap.putElement("1", "second");
				Thread.sleep(500);
				vmap.putElement("1", "third");
				Thread.sleep(500);
				vmap.putElement("1", "fourth");
				Thread.sleep(500);

				vmap.putElement("2", "first");
				Thread.sleep(500);
				vmap.putElement("2", "second");
				Thread.sleep(500);
				vmap.putElement("2", "third");
				Thread.sleep(500);
				vmap.putElement("2", "fourth");
				Thread.sleep(500);

				System.out.println(vmap.getElement("1"));
				System.out.println(vmap.getElement("2"));

				System.out.println();

				System.out.println(vmap.getElement("1", DateTime.now().minusSeconds(10).getMillis()));
				System.out.println(vmap.getElement("2", DateTime.now().minusSeconds(50).getMillis()));

				System.out.println();

				vmap.removeElement("1", DateTime.now().getMillis());
				vmap.removeElement("2", DateTime.now().getMillis());

				vmap.print();
		}
}
