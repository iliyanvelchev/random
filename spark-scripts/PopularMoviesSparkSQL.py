from pyspark.sql import SparkSession, functions, Row


def parseNames(line):
    fields = line.split('|')
    return (int(fields[0]), unicode(fields[1]).encode('ascii', 'ignore'))


def loadMovies(sc):
    movieNames = {}
    movies = sc.textFile("hdfs:///user/maria_dev/ml-100k/u.item")
    parsed = movies.map(parseNames).collect()
    for line in parsed:
        movieNames[int(line[0])] = line[1]
    return movieNames


def parseData(line):
    fields = line.split()
    return Row(movieID=int(fields[1]), rating=(float(fields[2])))


if __name__ == "__main__":
    spark = SparkSession.builder.appName("Popular Movies").getOrCreate()
    sc = spark.sparkContext
    # ( movieID | name | releaseDate | imdbLink )
    movieNames = loadMovies(sc)

    # ( userID | movieID | rating | ratingTime )
    lines = sc.textFile("hdfs:///user/maria_dev/ml-100k/u.data")

    # ( movieID | rating | 1.0 )
    movieRatings = lines.map(parseData)

    #Convert to DataFrame
    movieDataset = spark.createDataFrame(movieRatings)

    #Compute avg rating for each movie
    avgRatings = movieDataset.groupBy("movieID").avg("rating")

    #Compute count ratings for each movie
    counts = movieDataset.groupBy("movieID").count()

    #Join together (movieID,avg(rating),count)
    avgeragesAndCounts = counts.join(avgRatings, "movieID")

    #Join together (movieID,avg(rating),count)
    results = avgeragesAndCounts.orderBy("avg(rating)").take(10)

    #Print
    for result in results:
        print(movieNames[result[0]], result[1])

    spark.stop()
