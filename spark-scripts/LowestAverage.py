from pyspark import SparkConf, SparkContext


def parseNames(line):
    fields = line.split('|')
    return (int(fields[0]), unicode(fields[1]).encode('ascii', 'ignore'))


def loadMovies(sc):
    movieNames = {}
    movies = sc.textFile("hdfs:///user/maria_dev/ml-100k/u.item")
    parsed = movies.map(parseNames).collect()
    for line in parsed:
        movieNames[int(line[0])] = line[1]
    return movieNames


def parseData(line):
    fields = line.split()
    return (int(fields[1]), (float(fields[2]), 1.0))


if __name__ == "__main__":
    conf = SparkConf().setAppName("Worst Movies")
    sc = SparkContext(conf=conf)

    # ( movieID | name | releaseDate | imdbLink )
    movieNames = loadMovies(sc)

    # ( userID | movieID | rating | ratingTime )
    lines = sc.textFile("hdfs:///user/maria_dev/ml-100k/u.data")

    # ( movieID | rating | 1.0 )
    movieRatings = lines.map(parseData)

    # ( movieID | totalRatings | ratingsNum )
    movieRatingsTotal = movieRatings.reduceByKey(
        lambda movie1, movie2: (movie1[0] + movie2[0], movie1[1] + movie2[1])
    ).filter(lambda x: x[1][1] > 10.0)

    # (movieID | averageRating)
    avgRatings = movieRatingsTotal.mapValues(
        lambda totalAndCount: totalAndCount[0] / totalAndCount[1])

    sorted = avgRatings.sortBy(lambda x: x[1], 0)

    results = sorted.take(10)

    for result in results:
        print(movieNames[result[0]], result[1])
