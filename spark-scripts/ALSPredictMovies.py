from pyspark.sql import SparkSession, Row
from pyspark.sql.functions import lit
from pyspark.ml.recommendation import ALS


def parseNames(line):
    fields = line.split('|')
    return (int(fields[0]), unicode(fields[1]).encode('ascii', 'ignore'))


def loadMovies(sc):
    movieNames = {}
    movies = sc.textFile("hdfs:///user/maria_dev/ml-100k/u.item")
    parsed = movies.map(parseNames).collect()
    for line in parsed:
        movieNames[int(line[0])] = line[1]
    return movieNames


def parseData(line):
    fields = line.value.split()
    return Row(
        userID=int(fields[0]), movieID=int(fields[1]), rating=float(fields[2]))


if __name__ == "__main__":
    spark = SparkSession.builder.appName("Predict Movies").getOrCreate()
    sc = spark.sparkContext
    spark.conf.set("spark.sql.crossJoin.enabled", "true")
    # ( movieID | name | releaseDate | imdbLink )
    movieNames = loadMovies(sc)

    #Get Raw Data
    lines = spark.read.text("hdfs:///user/maria_dev/ml-100k/u.data").rdd

    #Creates RDD ( userID | movieID | rating )
    ratingsRDD = lines.map(parseData)

    #Convert to DataFrame and casche
    ratings = spark.createDataFrame(ratingsRDD).cache()

    #Create ALS model
    als = ALS(
        maxIter=5,
        regParam=0.1,
        userCol="userID",
        itemCol="movieID",
        ratingCol="rating")
    model = als.fit(ratings)

    print("\nRatings for UserID 0:")
    userRatings = ratings.filter("userID = 0")
    for rating in userRatings.collect():
        print(movieNames[rating["movieID"]], rating["rating"])

    print("\nTop 20 Recommendations")
    #Filter movies rated more than 100 times
    ratingCounts = ratings.groupBy("movieID").count().filter("count > 100")
    #Test DataFrame for user 0
    popularMovies = ratingCounts.select("movieID").withColumn("userID", lit(0))

    #Run model on user 0
    recommendations = model.transform(popularMovies)

    #Get top recommendations
    topRecommendations = recommendations.sort(
        recommendations.prediction.desc()).take(20)

    #Print
    for recommendation in topRecommendations:
        print(movieNames[recommendation["movieID"]],
              recommendation["prediction"])

    spark.stop()
