package designpatterns.factory;

public class Rectangle implements Shape
{
		private double height;

		private double width;

		public Rectangle(double height, double width)
		{
				this.height = height;
				this.width = width;
		}

		public double getHeight()
		{
				return height;
		}

		public double getWidth()
		{
				return width;
		}

		public String getAreaFormula()
		{
				return "S=a*b";
		}

		@Override
		public double getArea()
		{
				return width * height;
		}
}
