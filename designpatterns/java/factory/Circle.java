package designpatterns.factory;

public class Circle implements Shape
{
		private double radius;

		public Circle(double radius)
		{
				this.radius = radius;
		}

		public double getRadius()
		{
				return radius;
		}

		public String getAreaFormula()
		{
				return "S=3.14*r*r";
		}

		@Override
		public double getArea()
		{
				return 3.14 * radius * radius;
		}
}
