package designpatterns.factory.factory;

import designpatterns.factory.Rectangle;
import designpatterns.factory.Shape;

public class RectangleCreator extends ShapeCreator
{
		private double height;

		private double width;

		public RectangleCreator(double height, double width)
		{
				this.height = height;
				this.width = width;
		}

		@Override
		public Shape createShape()
		{
				return new Rectangle(height, width);
		}
}
