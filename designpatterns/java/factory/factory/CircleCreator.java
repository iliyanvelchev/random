package designpatterns.factory.factory;

import designpatterns.factory.Circle;
import designpatterns.factory.Shape;

public class CircleCreator extends ShapeCreator
{
		private double radius;

		public CircleCreator(double radius)
		{
				this.radius = radius;
		}

		@Override
		public Shape createShape()
		{
				return new Circle(radius);
		}
}
