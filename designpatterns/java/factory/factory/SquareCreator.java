package designpatterns.factory.factory;

import designpatterns.factory.Shape;
import designpatterns.factory.Square;

public class SquareCreator extends ShapeCreator
{
		private double side;

		public SquareCreator(double side)
		{
				this.side = side;
		}

		@Override
		public Shape createShape()
		{
				return new Square(side);
		}
}
