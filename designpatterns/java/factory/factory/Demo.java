package designpatterns.factory.factory;

import designpatterns.factory.Shape;

public class Demo
{
		public static void main(String[] args)
		{
				getShape(new SquareCreator(5));
				getShape(new CircleCreator(5));
				getShape(new RectangleCreator(4, 5));
		}

		private static Shape getShape(ShapeCreator creator)
		{
				return creator.getShape();
		}
}
