package designpatterns.factory.factory;

import designpatterns.factory.Shape;

public abstract class ShapeCreator
{
		public Shape getShape()
		{
				Shape shape = createShape();
				System.out.println("Shape : " + shape + " Area : " + shape.getArea());
				return shape;
		}

		public abstract Shape createShape();
}
