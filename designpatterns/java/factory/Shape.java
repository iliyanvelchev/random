package designpatterns.factory;

public interface Shape
{
		String getAreaFormula();

		double getArea();
}
