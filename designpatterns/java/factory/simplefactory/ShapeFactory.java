package designpatterns.factory.simplefactory;

import designpatterns.factory.Circle;
import designpatterns.factory.Rectangle;
import designpatterns.factory.Shape;
import designpatterns.factory.Square;

public class ShapeFactory
{
		public static Shape getShape(String type, double... params)
		{
				switch (type)
				{
						case "rectangle":
								return new Rectangle(params[0], params[1]);
						case "square":
								return new Square(params[0]);
						case "circle":
								return new Circle(params[0]);
						default:
								throw new IllegalArgumentException("No such shape");
				}
		}
}
