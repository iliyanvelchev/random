package designpatterns.factory.simplefactory;

import designpatterns.factory.Shape;

public class Demo
{
		public static void main(String[] args)
		{
				Shape rectangle = ShapeFactory.getShape("rectangle", 4, 5);
				System.out.println(rectangle.getAreaFormula());
				System.out.println(rectangle.getArea());

				Shape square = ShapeFactory.getShape("square", 5);
				System.out.println(square.getAreaFormula());
				System.out.println(square.getArea());

				Shape circle = ShapeFactory.getShape("circle", 5);
				System.out.println(circle.getAreaFormula());
				System.out.println(circle.getArea());

				ShapeFactory.getShape("unknown");
		}
}
