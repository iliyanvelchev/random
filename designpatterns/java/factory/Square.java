package designpatterns.factory;

public class Square implements Shape
{
		private double side;

		public Square(double side)
		{
				this.side = side;
		}

		public double getSide()
		{
				return side;
		}

		public String getAreaFormula()
		{
				return "S=a*a";
		}

		@Override
		public double getArea()
		{
				return side * side;
		}
}
