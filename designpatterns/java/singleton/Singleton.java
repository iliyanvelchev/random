package designpatterns.abstractfactory.singleton;

public class Singleton
{
		private static Singleton INSTANCE;

		private Singleton()
		{
		}

		public static Singleton getInstance()
		{
				if (INSTANCE != null)
						return INSTANCE;

				synchronized (Singleton.class)
				{
						if (INSTANCE == null)
								INSTANCE = new Singleton();
						return INSTANCE;
				}
		}
}
