package designpatterns.abstractfactory.singleton;

import java.util.HashSet;

public class Demo
{
		private static final HashSet<Singleton> SET = new HashSet<>();

		private static class Test implements Runnable
		{

				@Override
				public void run()
				{
						Singleton singleton = Singleton.getInstance();
						SET.add(singleton);
						System.out.println(singleton);
				}
		}

		public static void main(String[] args)
		{

				for (int i = 0; i < 15; i++)
						new Thread(new Test()).start();

				System.out.println("Singletons created : " + SET.size());
		}
}
