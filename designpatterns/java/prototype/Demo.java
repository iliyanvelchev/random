package designpatterns.prototype;

public class Demo
{
		public static void main(String[] args) throws CloneNotSupportedException
		{
				CloneableUnit u1 = new CloneableUnit();
				u1.setState(5);
				u1.setName("first");
				System.out.println(u1);

				CloneableUnit u2 = (CloneableUnit) u1.clone();
				System.out.println(u2);

				NonClonableUnit ncu = new NonClonableUnit();

				try
				{
						ncu.clone();
				}
				catch (CloneNotSupportedException e)
				{
						System.out.println("Not cloneable");
				}

		}
}
