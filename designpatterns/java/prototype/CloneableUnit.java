package designpatterns.prototype;

public class CloneableUnit extends Unit
{
		private Integer state;

		protected void reset()
		{
				state = 0;
		}

		public Integer getState()
		{
				return state;
		}

		public CloneableUnit setState(Integer state)
		{
				this.state = state;
				return this;
		}

		@Override public String toString()
		{
				return "CloneableUnit{" +
				       "state=" + state +
				       "} " + super.toString();
		}
}
