package designpatterns.prototype;

public abstract class Unit implements Cloneable
{
		private String name;

		public Unit clone() throws CloneNotSupportedException
		{
				Unit unit = (Unit) super.clone();
				unit.initialize();
				return unit;
		}

		private void initialize()
		{
				this.name = "";
				reset();
		}

		protected abstract void reset();

		public String getName()
		{
				return name;
		}

		public Unit setName(String name)
		{
				this.name = name;
				return this;
		}

		@Override public String toString()
		{
				return "Unit{" +
				       "name='" + name + '\'' +
				       '}';
		}
}
