package designpatterns.prototype;

public class NonClonableUnit extends Unit
{
		public Unit clone() throws CloneNotSupportedException
		{
				throw new CloneNotSupportedException("Single unit");
		}

		protected void reset()
		{
				throw new UnsupportedOperationException("Reset not supported");
		}
}
