package designpatterns.builder;


public class Demo
{
		public static void main(String[] args)
		{
				User.UserBuilder builder = User.getBuilder();
				User user = builder.withAddress("KirilPopov")
						.withAge(23)
						.withEmail("iliyan.velchev95@gmail.com")
						.withPassword("asdasdasd")
						.withUsername("iliyan95")
						.build();
				System.out.println(user);

				User user2 = builder.withAddress("KirilPopov35A")
						.withAge(23)
						.withEmail("iliyan.velchev95@gmail.com")
						.withPassword("asdasdasd")
						.withUsername("iliyan95")
						.build();
				System.out.println(user2);
		}
}
