package designpatterns.builder;

public class User
{
		private String username;

		private String address;

		private String password;

		private int age;

		private String email;

		private User()
		{

		}

		public String getUsername()
		{
				return username;
		}

		public String getAddress()
		{
				return address;
		}

		public String getPassword()
		{
				return password;
		}

		public int getAge()
		{
				return age;
		}

		public String getEmail()
		{
				return email;
		}

		private void setUsername(String username)
		{
				this.username = username;
		}

		private void setAddress(String address)
		{
				this.address = address;
		}

		private void setPassword(String password)
		{
				this.password = password;
		}

		private void setAge(int age)
		{
				this.age = age;
		}

		private void setEmail(String email)
		{
				this.email = email;
		}

		public String toString()
		{
				return "User{" +
				       "username='" + username + '\'' +
				       ", address='" + address + '\'' +
				       ", password='" + password + '\'' +
				       ", age=" + age +
				       ", email='" + email + '\'' +
				       '}';
		}

		public static UserBuilder getBuilder()
		{
				return new UserBuilder();
		}

		public static class UserBuilder
		{
				private User user;

				private UserBuilder()
				{
						this.user = new User();
				}

				public UserBuilder withUsername(String username)
				{
						user.setUsername(username);
						return this;
				}

				public UserBuilder withAddress(String address)
				{
						user.setAddress(address);
						return this;
				}

				public UserBuilder withEmail(String email)
				{
						user.setEmail(email);
						return this;
				}

				public UserBuilder withAge(int age)
				{
						user.setAge(age);
						return this;
				}

				public UserBuilder withPassword(String password)
				{
						user.setPassword(password);
						return this;
				}

				public User build()
				{
						return user;
				}
		}

}
