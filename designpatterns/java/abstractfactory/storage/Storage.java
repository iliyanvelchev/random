package designpatterns.abstractfactory.storage;

public interface Storage
{
		String getInfo();
}
