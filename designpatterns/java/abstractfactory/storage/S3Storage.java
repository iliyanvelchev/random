package designpatterns.abstractfactory.storage;

public class S3Storage implements Storage
{
		private int capMib;

		public S3Storage(int capMib)
		{
				this.capMib = capMib;
		}

		public String getInfo()
		{
				return "S3 storage ( " + capMib + " MB )";
		}

}
