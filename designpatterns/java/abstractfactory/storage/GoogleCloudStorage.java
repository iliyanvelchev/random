package designpatterns.abstractfactory.storage;

public class GoogleCloudStorage implements Storage
{
		private int capMib;

		public GoogleCloudStorage(int capMib)
		{
				this.capMib = capMib;
		}

		public String getInfo()
		{
				return "GoogleCloud Storage ( " + capMib + " MB )";
		}

}
