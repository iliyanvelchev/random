package designpatterns.abstractfactory;

import designpatterns.abstractfactory.instance.Instance;
import designpatterns.abstractfactory.resourcefactory.AwsResourceFactory;
import designpatterns.abstractfactory.resourcefactory.GoogleCloudResourceFactory;
import designpatterns.abstractfactory.resourcefactory.ResourceFactory;
import designpatterns.abstractfactory.storage.Storage;

public class Client
{
		private ResourceFactory factory;

		public Client(ResourceFactory factory)
		{
				this.factory = factory;
		}

		public Instance createServer(Instance.Capacity capacity, int capMib)
		{
				Storage storage = factory.createStorage(capMib);
				Instance instance = factory.createInstance(capacity, storage);
				return instance;
		}

		public static void main(String[] args)
		{
				Client aws = new Client(new AwsResourceFactory());
				Instance ec2 = aws.createServer(Instance.Capacity.SMALL, 2048);
				ec2.start();
				ec2.stop();
				System.out.println();
				Client gc = new Client(new GoogleCloudResourceFactory());
				Instance gce = gc.createServer(Instance.Capacity.BIG, 204800);
				gce.start();
				gce.stop();
		}
}
