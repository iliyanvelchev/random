package designpatterns.abstractfactory.instance;

import designpatterns.abstractfactory.storage.Storage;

public class EC2Instance implements Instance
{
		private Storage storage;

		private Capacity capacity;

		public EC2Instance(Capacity capacity, Storage storage)
		{
				if (storage == null)
						throw new RuntimeException("Storage must not be null");
				this.storage = storage;
				this.capacity = capacity;
				System.out.println(storage.getInfo() + " attached to " + getInfo());
		}

		public void start()
		{
				System.out.println(getInfo() + " with " + storage.getInfo() + " started...");
		}

		public void stop()
		{
				System.out.println(getInfo() + " with " + storage.getInfo() + " stopped.");
		}

		public String getInfo()
		{
				return capacity.toString() + " EC2 instance";
		}
}
