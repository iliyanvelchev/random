package designpatterns.abstractfactory.instance;

import designpatterns.abstractfactory.storage.Storage;

public interface Instance
{
		enum Capacity
		{
				SMALL, MEDIUM, BIG
		}

		void start();

		void stop();

		String getInfo();
}
