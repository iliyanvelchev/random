package designpatterns.abstractfactory.resourcefactory;

import designpatterns.abstractfactory.instance.GoogleCloudEngineInstance;
import designpatterns.abstractfactory.instance.Instance;
import designpatterns.abstractfactory.storage.GoogleCloudStorage;
import designpatterns.abstractfactory.storage.Storage;

public class GoogleCloudResourceFactory implements ResourceFactory
{
		public Storage createStorage(int capMib)
		{
				return new GoogleCloudStorage(capMib);
		}

		public Instance createInstance(Instance.Capacity capacity, Storage storage)
		{
				return new GoogleCloudEngineInstance(capacity, storage);
		}
}
