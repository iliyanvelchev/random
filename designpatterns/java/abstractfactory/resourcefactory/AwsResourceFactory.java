package designpatterns.abstractfactory.resourcefactory;

import designpatterns.abstractfactory.instance.EC2Instance;
import designpatterns.abstractfactory.instance.Instance;
import designpatterns.abstractfactory.storage.S3Storage;
import designpatterns.abstractfactory.storage.Storage;

public class AwsResourceFactory implements ResourceFactory
{
		public Storage createStorage(int capMib)
		{
				return new S3Storage(capMib);
		}

		public Instance createInstance(Instance.Capacity capacity, Storage storage)
		{
				return new EC2Instance(capacity, storage);
		}
}
