package designpatterns.abstractfactory.resourcefactory;

import designpatterns.abstractfactory.instance.Instance;
import designpatterns.abstractfactory.storage.Storage;

public interface ResourceFactory
{
		Storage createStorage(int capMib);

		Instance createInstance(Instance.Capacity capacity, Storage storage);
}
